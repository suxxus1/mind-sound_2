import React, { ReactElement } from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import {
  LoadingDataContext,
  SelectedPodcastContext,
  PodcastContext,
  EpisodesContext,
} from "../appContext";
import { Maybe, Nothing, Dict, Podcast, Episodes } from "../types";

export type Props = {
  route: string;
  isLoadingData: boolean;
  selectedEpisodeId: Maybe<number>;
  selectedPodcastId: Maybe<number>;
  podcast: Podcast;
  episodes: {
    episodesIds: Dict<Episodes>;
  };
};

function customRender(
  ui: ReactElement,
  {
    route = "",
    isLoadingData = false,
    selectedPodcastId = Nothing,
    selectedEpisodeId = Nothing,
    podcast = {
      data: {},
      ids: [],
    },
    episodes = { episodesIds: {} },
    ...renderOptions
  }: Partial<Props>
) {
  function wrapper({ children }: { children: React.ReactNode }) {
    return (
      <LoadingDataContext.Provider
        value={{
          isLoadingData,
        }}
      >
        <SelectedPodcastContext.Provider
          value={{
            selectedPodcastId,
            selectedEpisodeId,
          }}
        >
          <PodcastContext.Provider value={{ ...podcast }}>
            <EpisodesContext.Provider value={{ ...episodes }}>
              <MemoryRouter initialEntries={[route]}>{children}</MemoryRouter>
            </EpisodesContext.Provider>
          </PodcastContext.Provider>
        </SelectedPodcastContext.Provider>
      </LoadingDataContext.Provider>
    );
  }

  return render(ui, { wrapper, ...renderOptions });
}

export * from "@testing-library/user-event";
export * from "@testing-library/react";
export { customRender as render };
