import React, { useContext } from "react";
import styled from "styled-components";
import { title, subtitle, italic, TextColorGrey } from "../../styles/AppStyles";
import { Nothing } from "../../types";
import { Link } from "react-router-dom";
import { getPodDetail } from "../../utils/helpers";
import { PodcastContext, SelectedPodcastContext } from "../../appContext";

const Detail = styled.div`
  width: 300px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Description = styled.div`
  padding-left: 20px;
  width: 100%;
`;

const Author = styled.div`
  border: 1px solid var(--grey200);
  border-left: 0;
  border-right: 0;
  width: 100%;
  margin: 30px 0;
  padding: 20px;
  text-align: center;
`;

const Title = styled.h2`
  ${title}
`;

const Subtitle = styled.h2`
  ${subtitle}
`;

const Artist = styled.p`
  ${italic}
`;

const Parragraph = styled.p`
  ${TextColorGrey};
  ${italic};
`;

type Children = {
  children: React.ReactNode;
};

function DoLink({ children }: Children): JSX.Element {
  const { selectedPodcastId, selectedEpisodeId } = useContext(
    SelectedPodcastContext
  );

  const addLink =
    selectedEpisodeId !== Nothing && selectedPodcastId !== Nothing;

  if (addLink) {
    return (
      <Link
        data-testid="navigate-to-podcast"
        to={`/podcast/${selectedPodcastId}`}
        style={{ textAlign: "center", width: "100%" }}
      >
        {children}
      </Link>
    );
  }

  return <>{children}</>;
}

export default function PodcastDetail(): JSX.Element {
  const { data } = useContext(PodcastContext);
  const { selectedPodcastId } = useContext(SelectedPodcastContext);
  const { title, artist, description, img } = getPodDetail({
    data,
    id: selectedPodcastId,
  });

  if (selectedPodcastId !== Nothing) {
    return (
      <Detail data-testid="podcast-detail">
        <DoLink>
          <img alt={title} src={img} />
          <Author>
            <Title>{title}</Title>
            <Artist>{artist}</Artist>
          </Author>
        </DoLink>
        <Description>
          <Subtitle>Description:</Subtitle>
          <Parragraph>{description}</Parragraph>
        </Description>
      </Detail>
    );
  }

  return <></>;
}
