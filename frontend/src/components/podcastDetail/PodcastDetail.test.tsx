import React from "react";
import { render, screen } from "../testUtils";
import { PodcastWithOneAuthor } from "../../mocks/Fixtures";

import PodcastDetail from "./PodcastDetail";

describe("<PodscastDetail>", () => {
  //
  test("Podcast details, not in the document", () => {
    const { title, description, artist } = PodcastWithOneAuthor.data[2222];
    render(<PodcastDetail />, {
      selectedPodcastId: 2222,
    });

    screen.getByText(/description/i);
    expect(screen.queryByText(title)).not.toBeInTheDocument;
    expect(screen.queryByText(artist)).not.toBeInTheDocument;
    expect(screen.queryByText(description)).not.toBeInTheDocument;
    expect(screen.getByAltText(""));
  });

  test("Podcast details, not in the document", () => {
    const { title, description, artist } = PodcastWithOneAuthor.data[2222];
    render(<PodcastDetail />, {
      selectedPodcastId: 2222,
      podcast: {
        ...PodcastWithOneAuthor,
      },
    });

    screen.getByText(/description:/i);
    screen.getByText(title);
    screen.getByText(artist);
    screen.getByText(description);
    screen.getByAltText(title);
  });
});
