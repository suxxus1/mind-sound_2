import React, { useContext } from "react";
import { Link } from "react-router-dom";
import SearchPodByUser from "../searchBox/SearchPod";
import { Nothing } from "../../types";
import {
  ToggleThemeContext,
  LoadingDataContext,
  SelectedPodcastContext,
} from "../../appContext";
import styled, { keyframes } from "styled-components";
import { logoRGB } from "../../styles/AppStyles";

const fadeInFadeOut = keyframes`
  0% {
    opacity: 1;
  }
  50% {
    opacity: 0.2;
  }
  100% {
    opacity: 1;
  }
`;

const LoadingData = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: var(--bgsky400);
  animation-name: ${fadeInFadeOut};
  animation-duration: 5s;
  animation-iteration-count: infinite;
`;

const HeaderTop = styled.div`
  font-size: 24px;
  border-bottom: 1px solid var(--grey200);
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 10px;
  margin-bottom: 20px;
`;

const Logo = styled.div`
  background-repeat: no-repeat;
  width: 100px;
  height: 70px;
  ${logoRGB}
`;

const HeaderBottom = styled.div`
  display: flex;
  justify-content: right;
`;

export default function Header(): JSX.Element {
  const { isDark, setIsDark } = useContext(ToggleThemeContext);
  const { isLoadingData } = useContext(LoadingDataContext);
  const { selectedPodcastId } = useContext(SelectedPodcastContext);
  const showFilter = selectedPodcastId === Nothing;
  //
  return (
    <div data-testid="header">
      <HeaderTop>
        <h2>
          <Link data-testid="navigate-to-home" to="/">
            <Logo data-testid="logo"></Logo>
          </Link>
        </h2>
        <button
          onClick={() => {
            setIsDark(!isDark);
          }}
        >
          <i
            className={`fa fa-${isDark ? "sun" : "moon"}-o`}
            aria-hidden="true"
          ></i>
        </button>
        {isLoadingData && (
          <LoadingData data-testid="is-loading-data"></LoadingData>
        )}
      </HeaderTop>
      <HeaderBottom>{showFilter && <SearchPodByUser />}</HeaderBottom>
    </div>
  );
}
