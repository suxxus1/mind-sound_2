import React from "react";
import { render, screen } from "../testUtils";
import Header from "./Header";
//
describe("<Header> ", () => {
  const options = {
    isLoadingData: true,
  };
  //
  test("Is loading data, blue light is visible", () => {
    render(<Header />, {});
    expect(screen.queryByTestId("is-loading-data")).toBeNull();

    render(<Header />, { ...options });
    expect(screen.getByTestId("is-loading-data")).toBeTruthy();
  });

  test("The user can search by string", async () => {
    render(<Header />, {
      selectedPodcastId: 222,
    });
    expect(screen.queryByTestId("search-box")).not.toBeInTheDocument;

    render(<Header />, {});
    expect(screen.getByTestId("search-box")).toBeInTheDocument;
  });
});
