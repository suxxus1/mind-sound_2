import React, { useContext } from "react";
import styled from "styled-components";
import { boxShadow } from "../../styles/AppStyles";
import { PodId } from "../../types";
import { getPodDetail } from "../../utils/helpers";
import { PodcastContext } from "../../appContext";

type Id = { id: PodId };

const PodPreview = styled.div`
  width: 200px;
  height: 200px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 10px;
  ${boxShadow};
`;

const Metadata = styled.div`
  font-size: 12px;
  text-align: center;
`;

const MaskedImg = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50%;
`;

const Title = styled.h3`
  text-transform: uppercase;
  font-weight: 900;
  margin-bottom: 10px;
`;

const Artist = styled.h4`
  color: var(--bgslate500);
`;

export default function PodcastPreview({ id }: Id): JSX.Element {
  const { data } = useContext(PodcastContext);
  const { title, artist, img } = getPodDetail({
    data,
    id,
  });
  return (
    <PodPreview>
      <MaskedImg src={img} alt={title} />
      <Metadata>
        <Title>{title}</Title>
        <Artist>{artist}</Artist>
      </Metadata>
    </PodPreview>
  );
}
