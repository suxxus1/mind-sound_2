import React from "react";
import { render, screen } from "../testUtils";
import { PodcastWithOneAuthor } from "../../mocks/Fixtures";

import PodcastPreview from "./PodcastPreview";

describe("<PodcastDetail>", () => {
  //
  test("The user can see the Podcast details", async () => {
    const { title, artist } = PodcastWithOneAuthor.data[2222];
    render(<PodcastPreview id={2222} />, {});

    expect(screen.queryByText(title)).not.toBeInTheDocument;
    expect(screen.queryByText(artist)).not.toBeInTheDocument;
    expect(screen.queryByAltText(title)).not.toBeInTheDocument();

    render(<PodcastPreview id={2222} />, {
      podcast: {
        ...PodcastWithOneAuthor,
      },
    });

    screen.getByText(title);
    screen.getByAltText(title);
  });
});
