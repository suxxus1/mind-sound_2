import React, { useContext, useState, useMemo } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { scrollbar } from "../../styles/AppStyles";
import { SearchResult, SearchBy } from "../../types";
import { PodcastContext } from "../../appContext";
import {
  setSearchResult as getResultList,
  createSearchList,
} from "../../utils/helpers";

const Searcher = styled.div`
  display: flex;
  justify-content: space-between;
`;

const PodsNumber = styled.div`
  background-color: var(--bgsky400);
  color: white;
  padding: 5px;
  border-radius: 5px;
  margin-right: 10px;
  font-weight: bolder;
`;

const SearchBox = styled.div`
  position: relative;
`;

const Input = styled.input`
  padding: 5px;
  border: 1px solid var(--bgslate500);
  color: var(--grey500);
`;

const Ul = styled.ul`
  position: absolute;
  top: 35px;
  min-width: 240px;
  max-height: 400px;
  overflow-y: scroll;
  padding: 5px;
  background-color: var(--searchListBg);
  border: 1px solid var(--bgslate500);
  ${scrollbar}
`;

const Li = styled.li`
  padding: 5px;
  border-bottom: 1px solid var(--bgsky400);
  color: var(--grey500);
`;

export default function SearchPodByUser(): JSX.Element {
  //
  const podcasts = useContext(PodcastContext);
  const podsNumber = podcasts.ids.length;

  const [filterPodInput, setFilterPodInput] = useState("");
  const [searchResult, setSearchResult] = useState<SearchResult[]>([]);

  const searchList = useMemo<SearchBy[]>(
    () => createSearchList(podcasts),
    [podcasts]
  );

  return (
    <Searcher data-testid="search-box">
      <PodsNumber>{podsNumber}</PodsNumber>
      <SearchBox>
        <Input
          data-testid="search-input"
          type="text"
          value={filterPodInput}
          placeholder="filter podcast"
          onChange={(evt) => {
            const value = evt.target.value;
            setFilterPodInput(value);
            setSearchResult(getResultList(searchList, value));
          }}
        />
        {searchResult.length >= 1 && (
          <Ul data-testid="available-podcasts">
            {searchResult.map(({ id, label }: SearchResult) => (
              <Li key={id}>
                <Link to={`/podcast/${id}`}>{label}</Link>
              </Li>
            ))}
          </Ul>
        )}
      </SearchBox>
    </Searcher>
  );
}
