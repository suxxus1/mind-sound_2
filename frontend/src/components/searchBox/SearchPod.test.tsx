import React from "react";
import { render, screen, fireEvent } from "../testUtils";
import SearchPod from "./SearchPod";
import { PodcastWithOneAuthor } from "../../mocks/Fixtures";

const searchInputSetup = (props = {}) => {
  const utils = render(<SearchPod />, { ...props });
  const input: HTMLInputElement = screen.getByTestId("search-input");
  return {
    input,
    ...utils,
  };
};

//
describe("<SearchPod> ", () => {
  //
  describe("Postcad number", () => {
    test("No data, Podcast number equal 0", () => {
      render(<SearchPod />, {});
      screen.getByText("0");
    });

    test("Data fetched, Podcast number equal 10", () => {
      render(<SearchPod />, {
        podcast: {
          data: {},
          ids: Array(10).fill("value", 0),
        },
      });
      screen.getByText("10");
    });
  });

  describe("SearchBox", () => {
    test("The user enter text, no values match", () => {
      const { input } = searchInputSetup();
      expect(input.value).toBe("");

      fireEvent.change(input, { target: { value: "xyz" } });
      expect(input.value).toBe("xyz");
      //
      expect(screen.queryByTestId("available-podcasts")).not.toBeInTheDocument;
    });

    test("The user enter text, result list displayed", () => {
      const { input } = searchInputSetup({
        podcast: {
          ...PodcastWithOneAuthor,
        },
      });
      const title = /joe budden podcast/i;
      expect(input.value).toBe("");
      expect(screen.queryByText(title)).not.toBeInTheDocument;

      fireEvent.change(input, { target: { value: "p" } });
      expect(input.value).toBe("p");
      //
      expect(screen.getByText(title)).toBeInTheDocument;
    });
  });
});
