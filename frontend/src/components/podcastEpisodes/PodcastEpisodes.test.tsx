import React from "react";
import { render, screen } from "../testUtils";

import { PodEpisode } from "../../mocks/Fixtures";
import PodcastEpisodes from "./PodcastEpisodes";

const mockData = {
  selectedPodcastId: 1535809341,
  episodes: {
    episodesIds: {
      1535809341: {
        data: {
          1000604730376: {
            ...PodEpisode,
          },
        },
        ids: [1000604730376],
      },
    },
  },
};

describe("<PodcastEpisodes> ", () => {
  //
  test("Should render properly", () => {
    render(<PodcastEpisodes />, {});
    expect(screen.queryByText("episodes-list")).not.toBeInTheDocument;

    render(<PodcastEpisodes />, {
      ...mockData,
    });
    expect(screen.getByTestId("episodes-list")).toBeInTheDocument;
    screen.getByText(/Episodes:/i);
    screen.getByText(/Title/i);
    screen.getByText(/Date/i);
    screen.getByText(/Duration/i);
  });

  test("Should show the number of Episodes", () => {
    render(<PodcastEpisodes />, {});
    expect(screen.queryByText("episodes-list")).not.toBeInTheDocument;

    render(<PodcastEpisodes />, {
      ...mockData,
    });

    screen.getByText("Episodes: 1");
  });

  test("Should show the Epidode data", () => {
    render(<PodcastEpisodes />, {});
    expect(screen.queryByText("episodes-list")).not.toBeInTheDocument;

    render(<PodcastEpisodes />, {
      ...mockData,
    });

    const { trackName, duration, releaseDate } =
      mockData.episodes.episodesIds[1535809341].data[1000604730376];

    screen.getByText(trackName);
    screen.getByText(duration);
    screen.getByText(releaseDate);
  });

  test("Should exist Link to episode", () => {
    render(<PodcastEpisodes />, {});
    expect(screen.queryByText("episodes-list")).not.toBeInTheDocument;

    render(<PodcastEpisodes />, {
      ...mockData,
    });

    const { trackName, trackId } =
      mockData.episodes.episodesIds[1535809341].data[1000604730376];

    const href = `/podcast/${mockData.selectedPodcastId}/episode/${trackId}`;

    expect(screen.getByRole("link", { name: trackName })).toHaveAttribute(
      "href",
      href
    );
  });
});
