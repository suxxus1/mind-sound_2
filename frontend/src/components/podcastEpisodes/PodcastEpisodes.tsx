import React, { useContext } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { title, boxShadow } from "../../styles/AppStyles";

import { EpisodesContext, SelectedPodcastContext } from "../../appContext";
import { Nothing, PodcastsEpisodes } from "../../types";
import {
  getEpisodeByIdFromPodcastList,
  printConsoleMsg,
} from "../../utils/helpers";

const Container = styled.div`
  flex-grow: 1;
`;

const PodcastEpisodesTitleContainer = styled.div`
  margin-bottom: 20px;
  padding: 5px;
`;

const PodcastEpisodesTitle = styled.h1`
  ${title}
`;

const EpisodesList = styled.table`
  margin: 0 auto;
  font-size: 14px;
  color: var(--txtblue900);
  ${boxShadow};

  tbody tr:nth-child(odd) {
    border-bottom: 1px solid var(--borderRow);
    border-top: 1px solid var(--borderRow);
    background-color: var(--bgRow);
  }
`;

const Th = styled.th`
  color: var(--txtColorDefault);
  font-size: 16px;
  font-weight: 600;
  padding: 10px 5px;
`;

const ThFirst = styled(Th)`
  width: 75%;
`;

const ThLast = styled(Th)`
  text-align: center;
`;

const TdColor = `
    color: var(--episodesListTxtColor);
    & a {
      color: var(--episodesListTxtColor);
    }
`;
const Td = styled.td`
  padding: 10px 5px;
  ${TdColor}
`;

const TdLast = styled.td`
  text-align: center;
  ${TdColor}
`;

export default function PodcastEpisodes(): JSX.Element {
  const { selectedPodcastId } = useContext(SelectedPodcastContext);
  const podcastEpisodes: PodcastsEpisodes = useContext(EpisodesContext);

  const isSelectedPodcastId = selectedPodcastId !== Nothing;

  if (isSelectedPodcastId) {
    const episodes = getEpisodeByIdFromPodcastList(
      selectedPodcastId,
      podcastEpisodes
    );

    if (episodes === Nothing) return <></>;

    return (
      <Container data-testid="episodes-list">
        {episodes.ids.length >= 1 && (
          <>
            <PodcastEpisodesTitleContainer>
              <PodcastEpisodesTitle>
                Episodes: {episodes.ids.length}
              </PodcastEpisodesTitle>
            </PodcastEpisodesTitleContainer>
            <EpisodesList>
              <thead>
                <tr>
                  <ThFirst>Title</ThFirst>
                  <Th>Date</Th>
                  <ThLast>Duration</ThLast>
                </tr>
              </thead>
              <tbody>
                {episodes.ids.map((id) => {
                  const episode = episodes.data[id];
                  if (!episode) {
                    printConsoleMsg(`can not find an episode with id: ${id}`);
                  } else {
                    const { trackId, trackName, duration, releaseDate } =
                      episode;
                    return (
                      <tr key={trackId}>
                        <Td>
                          <Link
                            role="link"
                            to={`/podcast/${selectedPodcastId}/episode/${id}`}
                          >
                            {trackName}
                          </Link>
                        </Td>
                        <Td>{releaseDate}</Td>
                        <TdLast>{duration}</TdLast>
                      </tr>
                    );
                  }
                })}
              </tbody>
            </EpisodesList>
          </>
        )}
      </Container>
    );
  }
  return <></>;
}
