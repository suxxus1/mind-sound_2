import React from "react";
import styled from "styled-components";
import { mainContainer, container, scrollbar } from "../../styles/AppStyles";
import PodcastDetail from "../podcastDetail/PodcastDetail";
import EpisodeDetail from "../episodes/EpisodeDetail";

const ViewWrapper = styled.div`
  ${mainContainer}
  ${container}
  ${scrollbar}
`;

export default function EpisodeDetailView(): JSX.Element {
  return (
    <ViewWrapper data-testid="episode-view">
      <PodcastDetail />
      <EpisodeDetail />
    </ViewWrapper>
  );
}
