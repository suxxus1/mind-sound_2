import React, { useContext } from "react";
import styled from "styled-components";
import { mainContainer, container, scrollbar } from "../../styles/AppStyles";
import { Nothing } from "../../types";
import PodcastDetail from "../podcastDetail/PodcastDetail";
import PodcastEpisodes from "../podcastEpisodes/PodcastEpisodes";
import { SelectedPodcastContext } from "../../appContext";

const ViewWrapper = styled.div`
  ${mainContainer}
  ${container}
  ${scrollbar}
`;

export default function PodcastDetailView(): JSX.Element {
  const { selectedPodcastId } = useContext(SelectedPodcastContext);

  if (selectedPodcastId !== Nothing) {
    return (
      <ViewWrapper data-testid="podcast-view">
        <PodcastDetail />
        <PodcastEpisodes />
      </ViewWrapper>
    );
  }

  return <></>;
}
