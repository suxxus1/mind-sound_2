import React, { useContext } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { mainContainer, scrollbar } from "../../styles/AppStyles";
import { Podcast, Nothing } from "../../types";
import PodcastPreview from "../podcastPreview/PodcastPreview";
import { getPodcastById, printConsoleMsg } from "../../utils/helpers";
import { PodcastContext } from "../../appContext";

const ViewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  ${mainContainer}
  ${scrollbar}
`;

const Grid = styled.ul`
  margin-top: 50px;
  display: grid;
  grid-template-columns: auto auto auto auto;
  column-gap: 30px;
  row-gap: 30px;
`;

export default function MainView(): JSX.Element {
  const podcasts = useContext<Podcast>(PodcastContext);

  return (
    <ViewWrapper data-testid="main-view">
      <Grid data-testid="toppodcasts-list">
        {podcasts.ids.map((id) => {
          const pod = getPodcastById(id, podcasts.data);
          const isPod = pod !== Nothing;

          if (isPod) {
            return (
              <li key={id}>
                <Link
                  role="link"
                  to={`/podcast/${id}`}
                  data-testid="podcast-thumbnail"
                >
                  <PodcastPreview id={id} />
                </Link>
              </li>
            );
          } else {
            printConsoleMsg(`can not find podcast with id: ${id}`);
          }
        })}
      </Grid>
    </ViewWrapper>
  );
}
