import React from "react";
import { render, screen } from "../testUtils";
import { PodcastWithOneAuthor } from "../../mocks/Fixtures";

import { Podcast } from "../../types";
import MainView from "./MainView";

const podcast: Podcast = {
  ...PodcastWithOneAuthor,
};

describe("<MainView /> ", () => {
  test("Should render correctly", () => {
    render(<MainView />, {});

    screen.getByTestId("main-view");
    screen.getByTestId("toppodcasts-list");
  });

  test("Should show availble podscast list", () => {
    render(<MainView />, {});
    const { title, artist } = podcast.data[2222];
    const href = "/podcast/2222";

    expect(screen.queryByText(title)).not.toBeInTheDocument;
    expect(screen.queryByText(artist)).not.toBeInTheDocument;
    expect(screen.queryByRole("link")).not.toBeInTheDocument;

    render(<MainView />, {
      podcast,
    });

    screen.getByText(title);
    screen.getByText(artist);
    expect(screen.getByRole("link")).toHaveAttribute("href", href);
  });
});
