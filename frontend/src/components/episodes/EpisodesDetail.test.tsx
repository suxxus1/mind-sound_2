import React from "react";
import { render, screen } from "../testUtils";
import { PodEpisode } from "../../mocks/Fixtures";

import EpisodeDetail from "./EpisodeDetail";

describe("<EpisodeDetail>", () => {
  test("Epidode Details render correctly", async () => {
    render(<EpisodeDetail />, {
      selectedPodcastId: 1535809341,
      selectedEpisodeId: 1000604730376,
      episodes: {
        episodesIds: {
          1535809341: {
            data: {
              1000604730376: {
                ...PodEpisode,
              },
            },
            ids: [1000604730376],
          },
        },
      },
    });
    screen.getByText("Episode 611 | Butt Naked or Bounce");
    screen.getByText(/The JBP kicks off this episode recapping/i);
    screen.getByText("www.patreon.com/JoeBudden");
    screen.getByTestId("audio-ctrls");
  });
});
