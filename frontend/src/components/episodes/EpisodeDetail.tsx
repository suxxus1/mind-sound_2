import React, { useContext } from "react";
import {
  Maybe,
  Episode,
  PodcastsEpisodes,
  Episodes,
  Nothing,
} from "../../types";
import styled from "styled-components";
import {
  TextColorGrey,
  title,
  italic,
  boxShadow,
} from "../../styles/AppStyles";
import { getEpisodeById } from "../../utils/helpers";
import { SelectedPodcastContext, EpisodesContext } from "../../appContext";

const Detail = styled.div`
  padding: 30px;
  width: 700px;
  ${boxShadow};
`;

const Title = styled.h1`
  ${title};
  margin-bottom: 10px;
`;

const Description = styled.p`
  ${TextColorGrey}
  ${italic}
`;

const Audio = styled.audio`
  margin-top: 50px;
  width: 100%;
`;

export default function EpisodeDetail(): JSX.Element {
  const { selectedPodcastId, selectedEpisodeId } = useContext(
    SelectedPodcastContext
  );
  const podcastEpisodes = useContext<PodcastsEpisodes>(EpisodesContext);

  const isSelectedPodcastId = selectedPodcastId !== Nothing;
  const isSelectedEpisodeid = selectedEpisodeId !== Nothing;

  if (isSelectedPodcastId && isSelectedEpisodeid) {
    const episodes: Maybe<Episodes> = podcastEpisodes.episodesIds[
      selectedPodcastId
    ]?.ids.includes(selectedEpisodeId)
      ? podcastEpisodes.episodesIds[selectedPodcastId]
      : Nothing;

    const hasEpisodes = episodes !== Nothing;

    if (hasEpisodes) {
      const { data } = episodes as Episodes;
      const episodeDetail = getEpisodeById(selectedEpisodeId, data);
      if (episodeDetail !== Nothing) {
        const { trackName, description, audioSrc }: Episode = episodeDetail;
        return (
          <Detail data-testid="episode-detail">
            <Title>{trackName}</Title>
            <Description dangerouslySetInnerHTML={{ __html: description }} />
            {/* eslint-disable */}
            <Audio controls data-testid="audio-ctrls">
              <source src={audioSrc} />
            </Audio>
          </Detail>
        );
      }
    }
  }
  return <></>;
}
