import React from "react";
import styled from "styled-components";
import { logGreyScale } from "../../styles/AppStyles";

const Wrapper = styled.footer`
  border-top: 1px solid var(--grey500);
  padding: 10px;
  margin-top: 40px;

  font-size: 10px;
  color: var(--grey500);

  background-repeat: no-repeat;
  background-position: right;
  background-size: 25px;

  ${logGreyScale}
`;

const FooterName = styled.span`
  padding-left: 20px;
`;

export default function Footer(): JSX.Element {
  return (
    <Wrapper>
      <>
        © 2021 Copyright
        <FooterName>Mind Sound</FooterName>
      </>
    </Wrapper>
  );
}
