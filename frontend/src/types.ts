export const Nothing = Symbol("nothing");
type Nothing = typeof Nothing;

export type Maybe<T> = T | Nothing;

export type PodId = number;
export type EpisodeId = number;

// ------------------------
export type StorageData = Maybe<string>;

// ------------------------
//

export type SearchResult = {
  id: PodId;
  label: string;
};

export type Episode = {
  trackId: number;
  trackName: string;
  duration: string;
  releaseDate: string;
  description: string;
  audioType: string;
  audioSrc: string;
};

export type Dict<T> = Record<string, T>;

export type Episodes = {
  ids: number[];
  data: Dict<Episode>;
};

export type PodcastsEpisodes = {
  episodesIds: Dict<Episodes>;
};

export type Pod = {
  id: PodId;
  title: string;
  artist: string;
  description: string;
  img: string;
};

export type PodImg = {
  label: string;
  height: number;
};

export type SearchBy = {
  id: Maybe<PodId>;
  title: string;
  artist: string;
};

export type Podcast = {
  data: Dict<Pod>;
  ids: PodId[];
};
