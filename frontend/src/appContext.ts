import React, { createContext } from "react";
import {
  Podcast,
  Maybe,
  Nothing,
  PodId,
  PodcastsEpisodes,
  EpisodeId,
} from "./types";

type PodcastEpisodeSelected = {
  selectedPodcastId: Maybe<PodId>;
  selectedEpisodeId: Maybe<EpisodeId>;
};

type LoadingData = {
  isLoadingData: boolean;
};

type Theme = {
  setIsDark: React.Dispatch<React.SetStateAction<boolean>>;
  isDark: boolean;
};

export const ToggleThemeContext = createContext<Theme>({
  setIsDark: (a) => a,
  isDark: false,
});

export const LoadingDataContext = createContext<LoadingData>({
  isLoadingData: false,
});

export const PodcastContext = createContext<Podcast>({
  data: {},
  ids: [],
});

export const SelectedPodcastContext = createContext<PodcastEpisodeSelected>({
  selectedPodcastId: Nothing,
  selectedEpisodeId: Nothing,
});

export const EpisodesContext = createContext<PodcastsEpisodes>({
  episodesIds: {},
});
