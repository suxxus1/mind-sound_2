import React, { useState, useEffect } from "react";
import { Route, Routes, useLocation, matchPath } from "react-router-dom";
import styled, { ThemeProvider } from "styled-components";
import Header from "./components/header/Header";
import MainView from "./components/views/MainView";
import PodcastDetailView from "./components/views/PodcastDetailView";
import EpisodeDetailView from "./components/views/EpisodeDetailView";
import Footer from "./components/footer/Footer";
import {
  doOnStart,
  getEpisodes,
  numberFromString,
  getEpisodeByIdFromPodcastList,
  copyObject,
  controller,
} from "./utils/helpers";
import {
  Maybe,
  Nothing,
  Episodes,
  Podcast,
  PodcastsEpisodes,
  PodId,
  EpisodeId,
} from "./types";

import {
  ToggleThemeContext,
  PodcastContext,
  SelectedPodcastContext,
  LoadingDataContext,
  EpisodesContext,
} from "./appContext";

import { GlobalStyles } from "./styles/globalStyles";

const baseUrl = process.env.BASE_URL || "";
const URL_TOP_PODCASTS = `${baseUrl}/toppodcasts`;
const URL_EPISODES = `${baseUrl}/episodes/{{id}}`;

type PodcasIdEpisodId = {
  podcastId: Maybe<number>;
  episodeId: Maybe<number>;
};

const Container = styled.div`
  padding: 20px;
`;

function podcastIdEpisodeIdFromLocation(pathname: string): PodcasIdEpisodId {
  const matchPodcast = matchPath("/podcast/:podcastId", pathname);
  const matchEpisode = matchPath(
    "/podcast/:podcastId/episode/:episodeId",
    pathname
  );

  const podcastId: Maybe<number> = numberFromString(
    matchPodcast?.params?.podcastId || ""
  );

  const episodeId: Maybe<number> = numberFromString(
    matchEpisode?.params?.episodeId || ""
  );

  const podcastEpisode: Maybe<number> = numberFromString(
    matchEpisode?.params?.podcastId || ""
  );

  return {
    podcastId: podcastEpisode !== Nothing ? podcastEpisode : podcastId,
    episodeId,
  };
}

function App() {
  const [isDark, setIsDark] = useState<boolean>(false);
  const [podcasts, setPodcasts] = useState<Podcast>({
    data: {},
    ids: [],
  });
  const [isLoadingData, setIsLoadingData] = useState<boolean>(false);
  const [episodes, setEpisodes] = useState<PodcastsEpisodes>({
    episodesIds: {},
  });
  const [selectedPodcastId, setSelectedPodcastId] =
    useState<Maybe<PodId>>(Nothing);
  const [selectedEpisodeId, setSelectedEpisodeId] =
    useState<Maybe<EpisodeId>>(Nothing);

  const location = useLocation();

  useEffect(() => {
    setIsLoadingData(true);
    doOnStart(URL_TOP_PODCASTS).then((data) => {
      setIsLoadingData(false);
      setPodcasts(data);
    });
  }, []);

  useEffect(() => {
    const id: Maybe<number> = selectedPodcastId;

    if (id !== Nothing) {
      const podcastEpisodes = getEpisodeByIdFromPodcastList(id, episodes);
      const fetched = podcastEpisodes !== Nothing;

      if (fetched) return;

      const url = URL_EPISODES.replace("{{id}}", id.toString());

      setIsLoadingData(true);

      getEpisodes(url).then((data: Episodes) => {
        setIsLoadingData(false);
        //
        setEpisodes({
          episodesIds: {
            ...episodes.episodesIds,
            [selectedPodcastId]: data,
          },
        });
      });

      return () => {
        controller.abort();
      };
    }
  }, [selectedPodcastId, episodes]);

  useEffect(() => {
    //
    const { pathname } = location;
    const { podcastId, episodeId } = podcastIdEpisodeIdFromLocation(pathname);

    setSelectedPodcastId(podcastId);
    setSelectedEpisodeId(episodeId);
  }, [location]);

  return (
    <ThemeProvider theme={{ isDark }}>
      <Container data-testid="main-container">
        <GlobalStyles />
        <PodcastContext.Provider value={copyObject(podcasts) as Podcast}>
          <SelectedPodcastContext.Provider
            value={{
              selectedPodcastId: selectedPodcastId,
              selectedEpisodeId: selectedEpisodeId,
            }}
          >
            <LoadingDataContext.Provider
              value={{
                isLoadingData,
              }}
            >
              <ToggleThemeContext.Provider
                value={{
                  isDark,
                  setIsDark,
                }}
              >
                <Header />
              </ToggleThemeContext.Provider>
            </LoadingDataContext.Provider>
            <EpisodesContext.Provider
              value={copyObject(episodes) as PodcastsEpisodes}
            >
              <Routes>
                <Route path="/" element={<MainView />} />
                <Route
                  path="/podcast/:podcastId"
                  element={<PodcastDetailView />}
                />
                <Route
                  path="/podcast/:podcastId/episode/:episodeId"
                  element={<EpisodeDetailView />}
                />
              </Routes>
            </EpisodesContext.Provider>
          </SelectedPodcastContext.Provider>
        </PodcastContext.Provider>
        <Footer />
      </Container>
    </ThemeProvider>
  );
}
export default App;
