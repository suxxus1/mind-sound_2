import { Pod, Episode, Podcast, Nothing } from "../types";
import { ResultEpisode, Entry } from "../utils/jsonDecoder";

export const PodEpisode: Episode = {
  trackId: 1000604730376,
  trackName: "Episode 611 | Butt Naked or Bounce",
  duration: "0:25:30",
  releaseDate: "2023-02-18T08:00:00Z",
  description:
    '<p>The JBP kicks off this episode recapping their past few days before diving into their opinions on NBA All-Star Weekend in Utah (16:35). A video of YSL Woody allegedly snitching has been released which leads into a conversation surrounding street culture (50:46). Boosie cancels a potential collab album with T.I. (1:19:38), Jonathan Majors’ recent photoshoot for Ebony Magazine has sparked controversy (1:32:20), the advancement of artificial intelligence (2:15:30), + MORE!</p> <p><span>Become a Patron of The Joe Budden Podcast for additional bonus episodes and visual content for all things JBP.: Tap in here </span><span><a href="https://www.patreon.com/JoeBudden">www.patreon.com/JoeBudden</a></span></p> <p> </p> <p>Sleeper Picks:</p> <p>Joe | Nicole Bus - “Rain”</p> <p>Ice | Yung Bleu - “Games Women Play”</p> <p>Parks | Styles P - “Porsche Lights”</p> <p>Ish | Mario (feat. Ty Dolla $ign) - “Used To Me”</p> <p>Melyssa Ford | Th&amp;o. - “Prosecco”</p>',
  audioType: "audio/mpeg",
  audioSrc:
    "https://ia802203.us.archive.org/11/items/testmp3testfile/mpthreetest.mp3",
};

export const PodcastDetail: Pod = {
  img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
  id: 1535809341,
  title: "Joe Budden Podcast",
  description:
    "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
  artist: "The Joe Budden Network",
};

export const PreviewPod: Pod = {
  img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
  id: 2222,
  title: "Joe Budden Podcast",
  description:
    "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
  artist: "The Joe Budden Network",
};

export const PodcastWithOneAuthor: Podcast = {
  data: {
    2222: {
      ...PodcastDetail,
    },
  },
  ids: [2222],
};

export const PodcastWithNoEpisodes: Podcast = {
  data: {
    2222: {
      img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
      id: 2222,
      title: "Joe Budden Podcast",
      description:
        "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
      artist: "The Joe Budden Network",
    },
  },
  ids: [2222],
};

export const Podcasts: Podcast = {
  data: {
    2222: {
      ...PodcastDetail,
    },
    4442: {
      img: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/7b/cf/f6/7bcff6bb-5f99-6c2f-c6c5-3a9799f3df21/mza_8544742664200824246.jpg/170x170bb.png",
      id: 4442,
      title: "Million Dollaz Worth Of Game",
      description: "",
      artist: "Barstool Sports",
    },
    2223: {
      img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
      id: 2223,
      title: "Joe Budden Podcast",
      description: "",
      artist: "The Joe Budden Network",
    },
    5535: {
      img: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/7b/cf/f6/7bcff6bb-5f99-6c2f-c6c5-3a9799f3df21/mza_8544742664200824246.jpg/170x170bb.png",
      id: 5535,
      title: "Million Dollaz Worth Of Game",
      description: "",
      artist: "Barstool Sports",
    },
    3344: {
      img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
      id: 3344,
      title: "Joe Budden Podcast",
      description: "",
      artist: "The Joe Budden Network",
    },
    7765: {
      img: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/7b/cf/f6/7bcff6bb-5f99-6c2f-c6c5-3a9799f3df21/mza_8544742664200824246.jpg/170x170bb.png",
      id: 7765,
      title: "Million Dollaz Worth Of Game",
      description: "",
      artist: "Barstool Sports",
    },
    1119: {
      img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
      id: 1119,
      title: "Joe Budden Podcast",
      description: "",
      artist: "The Joe Budden Network",
    },
    3299: {
      img: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/7b/cf/f6/7bcff6bb-5f99-6c2f-c6c5-3a9799f3df21/mza_8544742664200824246.jpg/170x170bb.png",
      id: 3299,
      title: "Million Dollaz Worth Of Game",
      description: "",
      artist: "Barstool Sports",
    },
    8899: {
      img: "https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/170x170bb.png",
      id: 8899,
      title: "Joe Budden Podcast",
      description: "",
      artist: "The Joe Budden Network",
    },
    7766: {
      img: "https://is2-ssl.mzstatic.com/image/thumb/Podcasts125/v4/7b/cf/f6/7bcff6bb-5f99-6c2f-c6c5-3a9799f3df21/mza_8544742664200824246.jpg/170x170bb.png",
      id: 7766,
      title: "Million Dollaz Worth Of Game",
      description: "",
      artist: "Barstool Sports",
    },
  },
  ids: [2222, 4442, 2223, 5535, 3344, 7766, 8899, 3299, 7765, 1119],
};

export const episodesResult: ResultEpisode = {
  description: "description",
  releaseDate: "2023-02-22T12:18:19Z",
  trackId: 1000601100410,
  trackName: 'Episode 604 | "Enjoy Homeboy"',
  trackTimeMillis: 9490000,
  episodeFileExtension: "mp3",
  previewUrl: "http//...",
};

export const entry: Entry = {
  id: {
    attributes: {
      "im:id": "1535809341",
    },
  },
  "im:image": [
    {
      label: "im.png",
      attributes: {
        height: "170",
      },
    },
  ],
  "im:name": {
    label: "The Joe Budden Podcast",
  },
  "im:artist": {
    label: "The Joe Budden Network",
  },
  summary: {
    label:
      "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
  },
};

// JSON
// ===
//
//
// json from Apple Itumes Api
// GET -> https://itunes.apple.com/us/rss/toppodcasts/limit=1/genre=1310/json
export const json_one_entry = {
  feed: {
    entry: {
      "im:name": {
        label: "The Joe Budden Podcast",
      },
      "im:image": [
        {
          label: "im.png",
          attributes: {
            height: "170",
          },
        },
      ],
      "im:artist": {
        label: "The Joe Budden Network",
        attributes: {
          href: "https://podcasts.apple.com/us/artist/the-joe-budden-network/1535844019?uo=2",
        },
      },
      summary: {
        label:
          "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
      },
      id: {
        label:
          "https://podcasts.apple.com/us/podcast/the-joe-budden-podcast/id1535809341?uo=2",
        attributes: {
          "im:id": "1535809341",
        },
      },
    },
  },
};

// json from Apple Itumes Api
// GET -> https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json
export const json_with_entries = {
  feed: {
    entry: [
      {
        "im:name": {
          label: "The Joe Budden Podcast",
        },
        "im:image": [
          {
            label: "im.png",
            attributes: {
              height: "170",
            },
          },
        ],
        "im:artist": {
          label: "The Joe Budden Network",
          attributes: {
            href: "https://podcasts.apple.com/us/artist/the-joe-budden-network/1535844019?uo=2",
          },
        },
        summary: {
          label:
            "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        },
        id: {
          label:
            "https://podcasts.apple.com/us/podcast/the-joe-budden-podcast/id1535809341?uo=2",
          attributes: {
            "im:id": "1535809341",
          },
        },
      },
    ],
  },
};

// json from Apple Itunes Api
// https://itunes.apple.com/lookup?id=1535809341&media=podcast&entity=podcastEpisode
export const json_episodes = [
  {
    country: "USA",
    episodeUrl:
      "https://traffic.libsyn.com/secure/jbpod/Joe_Budden_Podcast_604_1.mp3?dest-id=2422538",
    artworkUrl600:
      "https://is2-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/600x600bb.jpg",
    genres: [
      {
        name: "Music",
        id: "1310",
      },
    ],
    episodeGuid: "da1030ff-da4a-49e8-bd4e-fd62ad93286f",
    description:
      "The JBP kicks off this episode recapping their past n released which leads into ...",
    releaseDate: "2023-02-22T12:18:19Z",
    trackId: 1000601100410,
    trackName: 'Episode 604 | "Enjoy Homeboy"',
    collectionId: 1535809341,
    collectionName: "The Joe Budden Podcast",
    closedCaptioning: "none",
    shortDescription:
      "The JBP kicks off this episode recapping their past few days before diving into their opinions on NBA All-Star Weekend in Utah (16:35). A video of YSL Woody allegedly snitching has been released which leads into a conversation surrounding street...",
    feedUrl: "https://jbpod.libsyn.com/applepodcast",
    trackViewUrl:
      "https://podcasts.apple.com/us/podcast/episode-604-enjoy-homeboy/id1535809341?i=1000601100410&uo=4",
    artworkUrl160:
      "https://is2-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/160x160bb.jpg",
    episodeContentType: "audio",
    previewUrl:
      "https://traffic.libsyn.com/secure/jbpod/Joe_Budden_Podcast_604_1.mp3?dest-id=2422538",
    artworkUrl60:
      "https://is2-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f2/21/fa/f221fabd-017f-5125-633b-f1fe4f39802a/mza_182995249085044287.jpg/60x60bb.jpg",
    artistViewUrl:
      "https://itunes.apple.com/us/artist/the-joe-budden-network/1535844019?mt=2&uo=4",
    contentAdvisoryRating: "Explicit",
    episodeFileExtension: "mp3",
    trackTimeMillis: 9490000,
    collectionViewUrl:
      "https://itunes.apple.com/us/podcast/the-joe-budden-podcast/id1535809341?mt=2&uo=4",
    kind: "podcast-episode",
    wrapperType: "podcastEpisode",
    artistIds: [1535844019],
  },
];

export const dataFromStorage = JSON.stringify({
  __date__: 1677346906601,
  __data__: json_with_entries.feed.entry,
});

export const dataFromStorageLessThan24Hs = JSON.stringify({
  __date__: 82800000,
  __data__: json_with_entries.feed.entry,
});

export const dataFromStorageLessThan24HsBadData = JSON.stringify({
  __date__: 333,
  __data__: [],
});
