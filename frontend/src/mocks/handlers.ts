import { rest } from "msw";
import topPodcasts from "./toppodcasts/data.json";
import episodes from "./episodes/data.json";

export const handlers = [
  rest.get("/toppodcasts", (_, res, ctx) =>
    res(ctx.status(200), ctx.json({ ...topPodcasts }))
  ),

  rest.get("/episodes/:id", (_, res, ctx) => {
    return res(ctx.status(200), ctx.json([...episodes]));
  }),
];
