import { createGlobalStyle, css } from "styled-components";
import resetCss from "./resetcss";

const font = `
  font-family: "Open Sans", sans-serif;
  font-size: 16px;
  font-synthesis: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-text-size-adjust: 100%;
`;

const commonVariables = `
    --sky: rgb(56 189 248);
    --bgslate100: rgb(241 245 249);
    --bgslate300: rgb(203 213 225);
    --bgslate500: rgb(100 116 139);
    --bgcyan50: rgb(236 254 255);
    --bgsky400: rgb(56 189 248);
    --grey200: rgb(229 231 235);
    --grey500: rgb(107 114 128);
    --txtblue500: rgb(59 130 246);
    --txtblue900: rgb(30 58 138);
    --txtColorDefault: #595858;
    --searchListBg: rgb(248, 248, 249);
`;

// episodes table
const episodesListLight = `
    --borderRow: rgb(203 213 225);
    --bgRow: rgb(241 245 249); 
    --episodesListTxtColor: rgb(29 71 121);
`;

// episodes table
const episodesListDark = `
    --borderRow: rgb(53 70 89);
    --bgRow: rgb(32 35 36); 
    --episodesListTxtColor: rgb(96 156 227);
 `;

const lightTheme = `
    ${commonVariables};
    --boxshadow: rgb(203, 213, 225);
    --primary-bg: #fff;
    ${episodesListLight}
  `;

const darkTheme = `
    ${commonVariables};
    --boxshadow: rgb(55, 55, 55);
    --primary-bg: rgb(21 23 23);
    --bgslate500: #888c88;
    --bgslate300: lime;
    --searchListBg: #212121;
     --txtColorDefault: rgb(180, 189, 180);
    ${episodesListDark}
`;

const global = css`
  ${resetCss}

  :root {
    ${font},
    ${(props) => {
      return props.theme.isDark ? darkTheme : lightTheme;
    }}
  }

  body {
    display: flex;
    justify-content: center;
    background-color: var(--primary-bg);
    color: var(--txtColorDefault);
  }

  button {
    cursor: pointer;
  }
`;

export const GlobalStyles = createGlobalStyle`
    ${global}
`;
