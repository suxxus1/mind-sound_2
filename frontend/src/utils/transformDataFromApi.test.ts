import {
  transformDataToEpisodesDataType,
  transformDataEntryToPod,
  getEpisodesFromResultEpisodesList,
  getPodscastsFromEntryList,
} from "./transformDataFromApi";

import { episodesResult, entry } from "../mocks/Fixtures";
import { Episode, Episodes, Pod, Podcast } from "../types";

test("Given ResultEpisode, should return Episode", () => {
  const actual = transformDataToEpisodesDataType(episodesResult);
  const expected: Episode = {
    description: "description",
    releaseDate: "2/22/2023",
    trackId: 1000601100410,
    trackName: 'Episode 604 | "Enjoy Homeboy"',
    duration: "02:38:10",
    audioType: "mp3",
    audioSrc: "http//...",
  };

  expect(episodesResult.description).toBe("description");
  expect(episodesResult.episodeFileExtension).toBe("mp3");
  expect(episodesResult.previewUrl).toBe("http//...");
  expect(episodesResult.releaseDate).toBe("2023-02-22T12:18:19Z");
  expect(episodesResult.trackId).toBe(1000601100410);
  expect(episodesResult.trackName).toBe('Episode 604 | "Enjoy Homeboy"');
  expect(episodesResult.trackTimeMillis).toBe(9490000);

  expect(actual.audioSrc).toBe(expected.audioSrc);
  expect(actual.audioType).toBe(expected.audioType);
  expect(actual.description).toBe(expected.description);
  expect(actual.duration).toBe(expected.duration);
  expect(actual.releaseDate).toBe(expected.releaseDate);
  expect(actual.trackId).toBe(expected.trackId);
  expect(actual.trackName).toBe(expected.trackName);
});

test("Given a list of ResultEpisode should return Episodes", () => {
  //
  const actual = getEpisodesFromResultEpisodesList([episodesResult]);
  const expected: Episodes = {
    ids: [1000601100410],
    data: {
      1000601100410: {
        description: "description",
        releaseDate: "2/22/2023",
        trackId: 1000601100410,
        trackName: 'Episode 604 | "Enjoy Homeboy"',
        duration: "02:38:10",
        audioType: "mp3",
        audioSrc: "http//...",
      },
    },
  };

  expect(episodesResult.description).toBe("description");
  expect(episodesResult.episodeFileExtension).toBe("mp3");
  expect(episodesResult.previewUrl).toBe("http//...");
  expect(episodesResult.releaseDate).toBe("2023-02-22T12:18:19Z");
  expect(episodesResult.trackId).toBe(1000601100410);
  expect(episodesResult.trackName).toBe('Episode 604 | "Enjoy Homeboy"');
  expect(episodesResult.trackTimeMillis).toBe(9490000);

  expect(actual).toMatchObject(expected);
});

test("Given Entry, should return Pod", () => {
  const actual = transformDataEntryToPod(entry);
  const expected: Pod = {
    id: 1535809341,
    title: "The Joe Budden Podcast",
    artist: "The Joe Budden Network",
    description:
      "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
    img: "im.png",
  };

  expect(entry.id.attributes["im:id"]).toBe("1535809341");
  expect(entry.summary.label).toBe(
    "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends."
  );
  expect(entry["im:artist"].label).toBe("The Joe Budden Network");
  expect(entry["im:image"]).toMatchObject([
    {
      label: "im.png",
      attributes: {
        height: "170",
      },
    },
  ]);
  expect(entry["im:name"].label).toBe("The Joe Budden Podcast");

  expect(actual.artist).toBe(expected.artist);
  expect(actual.description).toBe(expected.description);
  expect(actual.id).toBe(expected.id);
  expect(actual.img).toBe(expected.img);
  expect(actual.title).toBe(expected.title);
});

test("Given a list of Entry, should return Podcast", () => {
  //

  let actual = getPodscastsFromEntryList([entry]);
  let expected: Podcast = {
    ids: [1535809341],
    data: {
      1535809341: {
        id: 1535809341,
        title: "The Joe Budden Podcast",
        artist: "The Joe Budden Network",
        description:
          "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        img: "im.png",
      },
    },
  };

  expect(entry.id.attributes["im:id"]).toBe("1535809341");
  expect(entry.summary.label).toBe(
    "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends."
  );
  expect(entry["im:artist"].label).toBe("The Joe Budden Network");
  expect(entry["im:image"]).toMatchObject([
    {
      label: "im.png",
      attributes: {
        height: "170",
      },
    },
  ]);

  expect(entry["im:name"].label).toBe("The Joe Budden Podcast");
  expect(actual).toMatchObject(expected);

  const entryWithBadId = { ...entry, id: { attributes: { "im:id": "xxx" } } };

  expect(entryWithBadId.id.attributes["im:id"]).toBe("xxx");

  actual = getPodscastsFromEntryList([entryWithBadId]);
  expected = { ids: [], data: {} };

  expect(actual).toMatchObject(expected);
});
