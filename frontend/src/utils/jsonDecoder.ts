import * as D from "json-decoder";
import { numberFromString, printConsoleMsg } from "./helpers";
import { Maybe, Nothing } from "../types";

type Id = {
  attributes: {
    "im:id": string;
  };
};

export type Img = {
  label: string;
  attributes: { height: string };
};

type Name = {
  label: string;
};

type Artist = {
  label: string;
};

type Summary = {
  label: string;
};

export type Entry = {
  id: Id;
  "im:image": Img[];
  "im:name": Name;
  "im:artist": Artist;
  summary: Summary;
};

type Feed = {
  entry: unknown;
};

type JsonPopularPodcasts = {
  feed: Feed;
};

const IdDecoder = D.objectDecoder<Id>({
  attributes: D.objectDecoder({
    "im:id": D.stringDecoder,
  }),
});

const ImgDecoder = D.objectDecoder<Img>({
  label: D.stringDecoder,
  attributes: D.objectDecoder({
    height: D.stringDecoder,
  }),
});

const NameDecoder = D.objectDecoder<Name>({
  label: D.stringDecoder,
});

const ArtistDecoder = D.objectDecoder<Artist>({
  label: D.stringDecoder,
});

const SummaryDecoder = D.objectDecoder<Summary>({
  label: D.stringDecoder,
});

const EntryDecoder = D.objectDecoder<Entry>({
  id: IdDecoder,
  "im:image": D.arrayDecoder(ImgDecoder),
  "im:name": NameDecoder,
  "im:artist": ArtistDecoder,
  summary: SummaryDecoder,
});

const JsonDataDecoder = D.objectDecoder<JsonPopularPodcasts>({
  feed: D.objectDecoder<Feed>({
    entry: D.anyDecoder,
  }),
});

export function entryDecoder(data: unknown): Entry[] {
  const decoded = JsonDataDecoder.bind((r) => {
    const entry = r.feed.entry;
    const isAry = Array.isArray(entry);

    return D.valueDecoder(isAry ? entry : [entry]);
  })
    .bind((r) => {
      const decoded = D.arrayDecoder(EntryDecoder).decode(r);

      switch (decoded.type) {
        case "ERR":
          printConsoleMsg(decoded.message);
          return D.valueDecoder([]);
        case "OK":
          return D.valueDecoder(decoded.value);
      }
    })
    .decode(data);

  switch (decoded.type) {
    case "ERR":
      printConsoleMsg(decoded.message);
      return [];
    case "OK":
      return decoded.value;
  }
}

// JSON Episodes
// -----------
//
export type ResultEpisode = {
  description: string;
  releaseDate: string;
  trackId: number;
  trackName: string;
  trackTimeMillis: number;
  episodeFileExtension: string;
  previewUrl: string;
};

const EpisodeDecoder = D.objectDecoder<ResultEpisode>({
  description: D.stringDecoder,
  releaseDate: D.stringDecoder,
  trackId: D.numberDecoder,
  trackName: D.stringDecoder,
  trackTimeMillis: D.numberDecoder,
  episodeFileExtension: D.stringDecoder,
  previewUrl: D.stringDecoder,
});

export function episodeDecoder(data: unknown): ResultEpisode[] {
  //
  const decoded = D.arrayDecoder(EpisodeDecoder).decode(data);

  switch (decoded.type) {
    case "ERR":
      printConsoleMsg(decoded.message, "Warning");
      return [];
    case "OK":
      return decoded.value;
  }
}

// From localStorage
// -----------------
export type DataFromStorage = {
  __date__: Maybe<number>;
  __data__: Entry[];
};

export function dataStorageDecoder(data: unknown): DataFromStorage {
  const decoded = D.objectDecoder({
    __date__: D.numberDecoder,
    __data__: D.anyDecoder,
  })
    .bind((r) => {
      return D.valueDecoder({
        ...r,
        __data__: entryDecoder({
          feed: {
            entry: r.__data__,
          },
        }),
      });
    })
    .decode(data);

  const defaultdata: DataFromStorage = {
    __date__: Nothing,
    __data__: [],
  };

  switch (decoded.type) {
    case "ERR":
      printConsoleMsg(decoded.message);
      return defaultdata;
    case "OK":
      return (() => {
        const __date__ = numberFromString(decoded.value.__date__.toString());
        return {
          ...decoded.value,
          __date__,
        };
      })();
  }
}
