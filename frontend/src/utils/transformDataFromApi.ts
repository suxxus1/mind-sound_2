import { ResultEpisode, Entry } from "./jsonDecoder";
import { Episode, Episodes, Pod, Podcast, Maybe, Nothing } from "../types";
import sanitize from "sanitize-html";
import {
  getTheHighestValueFromHeigthAttr,
  getReadableDuration,
  toLocaleDate,
  numberFromString,
} from "./helpers";

export function transformDataToEpisodesDataType(
  resultEpisodes: ResultEpisode
): Episode {
  return {
    audioSrc: resultEpisodes.previewUrl,
    audioType: resultEpisodes.episodeFileExtension,
    description: sanitize(resultEpisodes.description),
    duration: getReadableDuration(resultEpisodes.trackTimeMillis),
    releaseDate: toLocaleDate(resultEpisodes.releaseDate, "en-US"),
    trackId: resultEpisodes.trackId,
    trackName: resultEpisodes.trackName,
  };
}

export function getEpisodesFromResultEpisodesList(
  resultEpisodes: ResultEpisode[]
): Episodes {
  return {
    ids: resultEpisodes.map(({ trackId }) => trackId),
    data: resultEpisodes.reduce(
      (acc, episodeResult) => ({
        ...acc,
        [episodeResult.trackId]: transformDataToEpisodesDataType(episodeResult),
      }),
      {}
    ),
  };
}

function getNumberFromEntryId({ id }: Entry): Maybe<number> {
  return numberFromString(id.attributes["im:id"]);
}

export function transformDataEntryToPod(entry: Entry): Pod {
  const id: Maybe<number> = getNumberFromEntryId(entry);
  return {
    id: id !== Nothing ? id : -1,
    title: entry["im:name"].label,
    artist: entry["im:artist"].label,
    description: entry.summary.label,
    img: getTheHighestValueFromHeigthAttr(entry["im:image"]).label,
  };
}

export function getPodscastsFromEntryList(entries: Entry[]): Podcast {
  const podcasts: Podcast = {
    ids: [],
    data: {},
  };

  const entriesLength = entries.length;
  let i = 0;

  while (i < entriesLength) {
    const tmpId: Maybe<number> = getNumberFromEntryId(entries[i]);

    if (tmpId !== Nothing) {
      podcasts.ids.push(tmpId);
      podcasts.data = {
        ...podcasts.data,
        [tmpId]: transformDataEntryToPod(entries[i]),
      };
    }
    i++;
  }

  return podcasts;
}
