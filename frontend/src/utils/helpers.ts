import axios from "axios";
import {
  Maybe,
  Nothing,
  Podcast,
  Pod,
  PodId,
  SearchBy,
  Episode,
  Episodes,
  PodImg,
  SearchResult,
  StorageData,
  PodcastsEpisodes,
  Dict,
} from "../types";

import {
  Entry,
  Img,
  DataFromStorage,
  dataStorageDecoder,
  entryDecoder,
  episodeDecoder,
} from "./jsonDecoder";
import {
  getPodscastsFromEntryList,
  getEpisodesFromResultEpisodesList,
} from "./transformDataFromApi";

type Lang = "en-US" | "es-ES";

function isNumber(value: unknown) {
  return typeof value === "number" && !isNaN(value);
}

export const printConsoleMsg =
  process.env.NODE_ENV === "development"
    ? console.error.bind(console)
    : (x: string) => x;

export function numberFromString(str: string): Maybe<number> {
  return isNumber(parseInt(str, 10)) ? parseInt(str, 10) : Nothing;
}

export function toLocaleDate(date: string, lang: Lang): string {
  const parsed = Date.parse(date);
  if (isNumber(parsed)) {
    return new Date(date).toLocaleDateString(lang);
  }
  printConsoleMsg(`Invalid format ${date}`);

  return "";
}

function padTo2Digits(num: number) {
  return num.toString().padStart(2, "0");
}

function formatDate(date: Date) {
  return [
    padTo2Digits(date.getUTCHours()),
    padTo2Digits(date.getUTCMinutes()),
    padTo2Digits(date.getUTCSeconds()),
  ].join(":");
}

export function getReadableDuration(milliseconds: number): string {
  if (isNumber(milliseconds)) {
    return formatDate(new Date(milliseconds));
  }

  printConsoleMsg(`expected a number type, got ${milliseconds}`);

  return "";
}

export function copyObject(obj: unknown): unknown {
  return JSON.parse(JSON.stringify(obj));
}

export function getPodcastById(
  id: number,
  dict: Record<string, Pod>
): Maybe<Pod> {
  return dict[id] || Nothing;
}

export function getEpisodeById(
  id: number,
  dict: Record<string, Episode>
): Maybe<Episode> {
  return dict[id] || Nothing;
}

export function getEpisodeByIdFromPodcastList(
  id: number,
  dict: PodcastsEpisodes
): Maybe<Episodes> {
  return dict.episodesIds[id] || Nothing;
}

function getValidPodcasts(podcasts: Maybe<Pod>[]): Pod[] {
  const podcastsValidated: Pod[] = [];
  podcasts.forEach((podcast) => {
    if (podcast !== Nothing) {
      podcastsValidated.push(podcast);
    }
  });
  return podcastsValidated;
}

function getAllPodcastsFromState({ ids, data }: Podcast): Maybe<Pod>[] {
  return ids.map((id: PodId) => getPodcastById(id, data));
}

export function createSearchList(podcasts: Podcast): SearchBy[] {
  return getValidPodcasts(getAllPodcastsFromState(podcasts)).map(
    ({ title, artist, id }) => ({
      id,
      artist,
      title,
    })
  );
}

export function addEpisodesToPodcast(podcasts: Podcast, podId: PodId): Podcast {
  const podcast: Maybe<Pod> = getPodcastById(podId, podcasts.data);
  const podcastsCopy = { ...podcasts };
  const isPodcast = podcast !== Nothing;

  if (isPodcast) {
    podcastsCopy.data[podcast.id] = podcast;
  } else {
    printConsoleMsg(`No podcast with id: ${podId} in podcasts`);
  }

  return podcastsCopy;
}

export function getTheHighestValueFromHeigthAttr(imgs: Img[]): PodImg {
  //
  const imagesWithHeight: PodImg[] = imgs.map((img) =>
    numberFromString(img.attributes.height) !== Nothing
      ? { label: img.label, height: Number(img.attributes.height) }
      : { label: img.label, height: 0 }
  );

  const highestValue = Math.max.apply(
    null,
    imagesWithHeight.map(({ height }) => height)
  );

  return (
    imagesWithHeight.filter((img) => img.height === highestValue).pop() || {
      label: "",
      height: 0,
    }
  );
}

export function setSearchResult(
  searchList: SearchBy[],
  str: string
): SearchResult[] {
  const minLength = 1;
  const searchListLength = searchList.length;

  let i = 0;

  const result: SearchResult[] = [];

  const toFind = str.toLowerCase();

  if (str.length >= minLength) {
    while (i < searchListLength) {
      const searchResult: SearchBy = searchList[i];
      const title = searchResult?.title.toLowerCase() || "";
      const artist = searchResult?.artist.toLowerCase() || "";
      const id = searchResult?.id !== Nothing ? searchResult.id : -1;

      const found = id > 0;

      if (found) {
        const isInTitle = title.includes(toFind);
        const isInArtist = artist.includes(toFind);

        if (isInTitle) {
          result.push({ id, label: searchResult.title });
        } else if (isInArtist) {
          result.push({ id, label: searchResult.artist });
        }
      }

      i++;
    }
  }
  return result;
}

export const STORAGE_PODCAST_DATA = "__STORAGE_PODCAST_DATA__";

export function isLessThan24HsElapsed(startTime: number): boolean {
  const endTime = window.performance.now();
  const elapsed = startTime - endTime;

  const h24 = 86400000;
  return elapsed < h24;
}

export function setItem(key: string, value: string) {
  localStorage.setItem(key, value);
}

export function getItem(key: string) {
  return localStorage.getItem(key);
}

export function removeItem(key: string) {
  localStorage.removeItem(key);
}

export function getJsonFromStorage(key: string): Maybe<StorageData> {
  return getItem(key) || Nothing;
}

export function getPodcatsFromLocalStorage(data: string): Podcast {
  const dataDecoded: DataFromStorage = dataStorageDecoder(JSON.parse(data));
  const entryDecoded: Entry[] = entryDecoder({
    feed: {
      entry: dataDecoded.__data__,
    },
  });
  return getPodscastsFromEntryList(entryDecoded);
}

export let controller = new AbortController();
// Api get
export async function doGet(url: string): Promise<unknown> {
  controller = new AbortController();
  try {
    const result = await axios.get(url, {
      signal: controller.signal,
      headers: {
        "Cache-Control": "no-cache",
      },
    });

    return result.data;

    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  } catch (e: any) {
    if (e.name === "CanceledError") {
      console.warn("request Cancelled");
    } else {
      console.error(e?.message);
    }
    return Nothing;
  }
}

export function doOnStart(url: string): Promise<Podcast> {
  const dataFromStorage = getJsonFromStorage(STORAGE_PODCAST_DATA);
  const isDataFromStorage = dataFromStorage !== Nothing;

  if (isDataFromStorage) {
    const data = JSON.parse(dataFromStorage);
    const date: Maybe<number> = numberFromString(data.__date__);
    const dateIsNumber = date !== Nothing;

    if (dateIsNumber && isLessThan24HsElapsed(date)) {
      if (Array.isArray(data.__data__) && data.__data__.length >= 1) {
        return Promise.resolve(getPodcatsFromLocalStorage(dataFromStorage));
      }
    }
  }

  return doGet(url).then((data) => {
    const entryDecoded = entryDecoder(data);

    const dataToStore: DataFromStorage = {
      __date__: window.performance.now(),
      __data__: entryDecoded,
    };

    setItem(STORAGE_PODCAST_DATA, JSON.stringify(dataToStore));
    return getPodscastsFromEntryList(entryDecoded);
  });
}

export function getEpisodes(url: string): Promise<Episodes> {
  return doGet(url).then((data: unknown) => {
    const decoded = episodeDecoder(data);
    return getEpisodesFromResultEpisodesList(decoded);
  });
}

export function getPodDetail({
  data,
  id,
}: {
  data: Dict<Pod>;
  id: Maybe<number>;
}): Pod {
  const defaultState = {
    title: "",
    artist: "",
    description: "",
    img: "",
    id: -1,
  };

  if (id !== Nothing) {
    const podcastDetail = getPodcastById(id, data);
    if (podcastDetail !== Nothing) {
      return podcastDetail;
    }
  } else {
    printConsoleMsg("expected id type to be a number, got Nothing");
  }

  return defaultState;
}
