import {
  json_one_entry,
  json_with_entries,
  json_episodes,
  dataFromStorage,
} from "../mocks/Fixtures";
import {
  entryDecoder,
  episodeDecoder,
  Entry,
  ResultEpisode,
  dataStorageDecoder,
} from "./jsonDecoder";

test("Given popular json data, should validate needed data", () => {
  let actual: Entry = entryDecoder(json_one_entry)[0];
  const expected = {
    "im:name": {
      label: "The Joe Budden Podcast",
    },
    "im:image": [
      {
        label: "im.png",
        attributes: {
          height: "170",
        },
      },
    ],
    "im:artist": {
      label: "The Joe Budden Network",
    },
    summary: {
      label:
        "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
    },
    id: {
      attributes: {
        "im:id": "1535809341",
      },
    },
  };

  expect(actual).toMatchObject(expected);

  actual = entryDecoder(json_with_entries)[0];

  expect(actual).toMatchObject(expected);
});

test("Given episodes json data, should validate the needed data", () => {
  const actual = episodeDecoder(json_episodes);
  const expected: ResultEpisode[] = [
    {
      description:
        "The JBP kicks off this episode recapping their past n released which leads into ...",
      releaseDate: "2023-02-22T12:18:19Z",
      trackId: 1000601100410,
      trackName: 'Episode 604 | "Enjoy Homeboy"',
      trackTimeMillis: 9490000,
      episodeFileExtension: "mp3",
      previewUrl:
        "https://traffic.libsyn.com/secure/jbpod/Joe_Budden_Podcast_604_1.mp3?dest-id=2422538",
    },
  ];
  expect(actual).toStrictEqual(expected);
});
//
test("Given Json data from Storage should decode the needed data", () => {
  const actual = dataStorageDecoder(JSON.parse(dataFromStorage));
  //
  const expected = {
    __date__: 1677346906601,
    __data__: [
      {
        "im:name": {
          label: "The Joe Budden Podcast",
        },
        "im:image": [
          {
            label: "im.png",
            attributes: {
              height: "170",
            },
          },
        ],
        "im:artist": {
          label: "The Joe Budden Network",
        },
        summary: {
          label:
            "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        },
        id: {
          attributes: {
            "im:id": "1535809341",
          },
        },
      },
    ],
  };

  const j = JSON.stringify({
    __date__: 1677346906601,
    __data__: [
      {
        "im:name": {
          label: "The Joe Budden Podcast",
        },
        "im:image": [
          {
            label: "im.png",
            attributes: {
              height: "170",
            },
          },
        ],
        "im:artist": {
          label: "The Joe Budden Network",
          attributes: {
            href: "https://podcasts.apple.com/us/artist/the-joe-budden-network/1535844019?uo=2",
          },
        },
        summary: {
          label:
            "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        },
        id: {
          label:
            "https://podcasts.apple.com/us/podcast/the-joe-budden-podcast/id1535809341?uo=2",
          attributes: {
            "im:id": "1535809341",
          },
        },
      },
    ],
  });

  expect(dataFromStorage).toBe(j);
  expect(actual.__date__).toBe(expected.__date__);
  expect(actual.__data__).toStrictEqual(expected.__data__);
});
