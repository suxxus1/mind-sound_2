import type {} from "cypress";

describe("Mind Sound", () => {
  const isProd = Cypress.env("mode") === "production";
  before(() => {
    if (isProd) {
      cy.log("production");
      const podcasts = Cypress.env("get_podcasts");
      const episodes = Cypress.env("get_episodes");
      // ----------------------------------------------------
      cy.intercept("GET", podcasts).as("toppodcasts");
      cy.intercept("GET", episodes).as("episodes");
    }
    cy.visit("/");
  });

  const msShort = 1000;

  describe("Mind Sound all Views", () => {
    const n = Math.floor(Math.random() * 5);

    const doWithToppodcastsData = () => {
      cy.get("[data-testid=search-input]").should("to.exist");
      cy.get("[data-testid=search-input]").clear();
    };

    const doWithEpisodesData = () => {
      cy.get("[data-testid=episodes-list]").should("to.exist");
      cy.get("[data-testid=episodes-list] tr a").eq(2).should("to.exist");
      cy.get("[data-testid=episodes-list] tr a").eq(2).click();

      /* eslint-disable cypress/no-unnecessary-waiting */
      cy.wait(msShort).then(() => {
        cy.get("[data-testid=audio-ctrls]").should("to.exist");
      });
      cy.get("[data-testid=navigate-to-podcast]").click();
      cy.get("[data-testid=navigate-to-home]").should("to.exist");
      cy.get("[data-testid=navigate-to-home]").click();
      cy.get("[data-testid=podcast-thumbnail]").eq(n).should("to.exist");
      cy.get("[data-testid=podcast-thumbnail]").eq(n).click();
    };

    const availablePodcasts = () => {
      cy.get("[data-testid=available-podcasts]  li").eq(n).should("to.exist");
      cy.get("[data-testid=available-podcasts]  li").eq(n).click();
    };

    const userInput = () => {
      /* eslint-disable cypress/no-unnecessary-waiting */
      cy.wait(msShort).then(() => {
        cy.get("[data-testid=search-input]").type("p");
      });
    };

    it("The user searches for a podcast to listen to", () => {
      if (isProd) {
        cy.wait("@toppodcasts").then(() => {
          doWithToppodcastsData();
        });
        userInput();
        availablePodcasts();
        cy.wait("@episodes").then(() => {
          doWithEpisodesData();
        });
      } else {
        // on development we mock the request.
        // we do not need to wait
        doWithToppodcastsData();
        userInput();
        availablePodcasts();
        doWithEpisodesData();
      }
    });
  });
});
