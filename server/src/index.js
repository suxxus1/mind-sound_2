import express from "express";
import cors from "cors";

const app = express();

const URL_TOP_PODCASTS =
  "https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json";

const URL_EPISODES =
  "https://itunes.apple.com/lookup?id={{id}}&media=podcast&entity=podcastEpisode";

async function doGet(url) {
  try {
    const result = await fetch(url);
    const json = await result.json();
    return json;
  } catch (e) {
    console.error(e?.message);
  }
}

app.use(
  cors({
    origin: "*",
  })
);

app.get("/toppodcasts", async (_, res) => {
  try {
    const data = await doGet(URL_TOP_PODCASTS);
    res.send(data);
  } catch (e) {
    console.error(e.message);
  }
});

app.get("/episodes/:id", async (req, res) => {
  try {
    const urlEpisodes = URL_EPISODES.replace("{{id}}", req.params.id);
    const json = await doGet(
      `https://api.allorigins.win/get?url=${encodeURIComponent(urlEpisodes)}`
    );
    const contents = JSON.parse(json.contents);

    // remove first entry, since is not valid
    const r = JSON.parse(JSON.stringify(contents));
    Array.isArray(r.results) ? r.results.splice(0, 1) : [];

    res.send(r.results);
  } catch (e) {
    console.error("error ->", e.message);
  }
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.info("listening on " + port));
