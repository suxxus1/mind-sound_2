# [Mind Sound](https://mindsound2.onrender.com/)

<div align="center">
  <a href="https://mind-sound.onrender.com/">
    <img src="markdown/logo-rgb.svg" width="200" height="200" />
  </a>
</div>

### Front-end technical exercise

The objective of this Exercise is to build an app to listen to podcasts. Content is retrieved from the Apple Itunes api.

[WIKI: description of the exercise](https://gitlab.com/suxxus1/podcast-react/-/wikis/Podcast)

### React:

**_There are 2 versions_**:

#### Stateful components

using

- useContext (for passing data)
- useState hook
- useMemo hook
- ThemeProvider

[![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)

#### Stateless components

[mind sound repo](https://gitlab.com/suxxus1/podcast-react)

using

- useReducer hook
- Single state
- Prop drilling, passing data to components
- Sending messages to update state

**Both versions have pros and cons, it's good to compare both solutions.**

## Clone repo:

```
git clone "https://gitlab.com/suxxus1/podcast-react.git"

```

## Develop:

./frontend/[README](frontend)

./server/[README](server)

## Gitlab-ci:

Documentation is available at ./Gitlab-ci.md

## Technology:

- **Webpack**
- **Storybook** (staleless version only)
- **Typescript**
- **Jest**
- **React testing library** (React component testing)
- **MSW** (to mock requests)
- **Cypress** (E2E testing)
- **Docker**
  - to develop the app
  - to run the compiled version locally (frontend & server)
  - to run Gitlab-ci locally

## Resources:

<div>
  <img src="markdown/render.svg" width="15" height="15" />
  [render.com](https://render.com/)
</div>

## Demo online:

[Mind Sound](https://mindsound2.onrender.com/)
